﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Events.Interfaces;
using UnityEngine;

namespace Assets.Scripts.Events {
    public class ReceivesEvents : MonoBehaviour {

    }

    public class EventManager : MonoBehaviour
    {
        /// <summary>
        /// Holds the events used in Register()
        /// </summary>
        Dictionary<Type, List<Action<IEvent>>> eventDictionary = new Dictionary<Type, List<Action<IEvent>>>();

        /// <summary>
        /// Holds the events used in RegisterThing()
        /// </summary>
        Dictionary<Type, List<object>> otherEventDictionary = new Dictionary<Type, List<object>>();
        
        #region Singleton Region

        private static EventManager _instance;

        public static EventManager Instance {
            get {
                return _instance ?? (_instance = new EventManager());
            }
        }

        #endregion

        #region Unity Methods Region

        void Awake() {
            var recievesEvents = FindObjectsOfType<ReceivesEvents>();
            foreach (var recievesEvent in recievesEvents) {
                var recieverType = recievesEvent.GetType();
                //recieverType.GetInterfaces().Where(x => x.Name == "")
                foreach (var interfaceType in recieverType.GetInterfaces()
                    .Where(x => x.IsGenericType && x.IsAssignableFrom(typeof(IEventHandler<>)))) {
                    // get generic types -> these will be the event types
                    // register object for each event type
                }
            }
        }

        #endregion

        #region Register Events Region

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventCallback"></param>
        public void Register<T>(Action<T> eventCallback) where T : IEvent {
            Register(typeof(T), (IEvent e) => eventCallback((T)e));
        }

        // (IEvent e) => eventCallback((T)e) written by GitHub user Togusa09 [https://github.com/Togusa09/].  It breaks my brain at the moment but it makes 
        // the Register() method so much easier to use as demonstrated by the change in the Register method in the HelloWorld class. 

        public void Register(Type eventType, Action<IEvent> eventCallback) {

            if (!eventType.IsAssignableFrom(eventType)) throw new Exception("Needs to implement IEvent");

            if (eventDictionary.ContainsKey(eventType)) {
                var callbackList = eventDictionary[eventType];
                callbackList.Add(eventCallback);
            }
            else {
                var callbackList = new List<Action<IEvent>>();
                callbackList.Add(eventCallback);
                eventDictionary.Add(eventType, callbackList);
            }
        }

        /// <summary>
        /// Register an event using a class that implements IEvent.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventHandler">The eventHand</param>
        public void RegisterThing<T>(IEventHandler<T> eventHandler) where T : IEvent {
            
            var eventType = typeof(T);

            if (otherEventDictionary.ContainsKey(eventType)) {
                var callbackList = otherEventDictionary[eventType];
                callbackList.Add(eventHandler);
            }
            else {
                var callbackList = new List<object>();
                callbackList.Add(eventHandler);
                otherEventDictionary.Add(eventType, callbackList);
            }
        }

        #endregion

        #region Raise Event Region
        public void RaiseThing<T>(T @event) where T : IEvent {
            var callbackList = otherEventDictionary[typeof(T)];
            var typeCallbacks = callbackList.OfType<IEventHandler<T>>();
            foreach (var callback in typeCallbacks) {
                callback?.Handle(@event);
            }
        }

        // Raise the event.
        public void Raise<T>(T eventValue) where T : IEvent {
            var type = typeof(T);

            if (!eventDictionary.ContainsKey(type)) return;

            var callbackList = eventDictionary[type];
            foreach (var callback in callbackList) {
                callback?.Invoke(eventValue);
            }
        }

        #endregion
        
        /// <summary>
        /// Generic version to unsubscribe from an event
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventCallback"></param>
        public void Unsubscribe<T>(Action<IEvent> eventCallback) where T : IEvent {
            Unsubscribe(typeof(T), eventCallback);
        }

        /// <summary>
        /// Unsubscribe from an event.  
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="eventCallback"></param>
        public void Unsubscribe(Type eventType, Action<IEvent> eventCallback) {
            if (!eventType.IsAssignableFrom(eventType)) throw new Exception("Needs to implement IEvent");

            if (eventDictionary.ContainsKey(eventType)) {
                eventDictionary.Remove(eventType);
            }
            else throw new Exception("Event does not exist");
        }

        public void UnsubscribeThing<T>(IEventHandler<T> eventHandler) where T : IEvent {
            var eventType = typeof(T);

            if (otherEventDictionary.ContainsKey(eventType)) {
                var callbackList = otherEventDictionary[eventType];
                if (callbackList.Count <= 0) throw new Exception("Event does not exist");
                callbackList.Remove(eventHandler);
            }
            else throw new Exception("Event does not exist");
        }
    }
}